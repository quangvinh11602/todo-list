/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    "./src/**/*.{js,jsx,ts,tsx}",
  ],
  theme: {
    extend: {
      fontFamily: {
        cabin: ["Cabin", "sans-serif"],
        inter: ['"Inter"', "sans-serif"],
      },
      backgroundImage: {
        background_gradient_2:
          "linear-gradient(125deg, #4585bd 4%, #83ccc4 73%);",
         
      },
      boxShadow:{
        shadow_2: "4px 6px 14px 2px #0000002e"
      }
    },
  },
  plugins: [],
}