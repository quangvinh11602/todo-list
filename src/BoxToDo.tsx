import { useEffect, useRef, useState } from 'react'
import { useTodoContext } from './TodoContext'
import { v4 as uuidv4 } from 'uuid';

interface Todo {
  id: string
  name: string
}

export default function BoxToDo() {
  const { todos, setTodos, setCompletedTodos } = useTodoContext();
  const [newTodo, setNewTodo] = useState<string>('');
  const [editingTodoId, setEditingTodoId] = useState<string | null>(null);
  const inputRef = useRef<HTMLInputElement | null>(null);

  useEffect(() => {
   
    if (editingTodoId !== null && inputRef.current) {
      inputRef.current.focus();
    }
  }, [editingTodoId]);
  const handleAddTodo = () => {
    if (newTodo.trim() !== '') {
      const newTodoItem: Todo = {
        id: uuidv4(),
        name: newTodo,
      };
      setTodos((prevTodos) => [...prevTodos, newTodoItem]);
      setNewTodo('');
    }
  };

  const handleDeleteTodo = (id: string) => {
    const updatedTodos = todos.filter((todo) => todo.id !== id);
    setTodos(updatedTodos);
  };

  const handleCheckTodo = (id: string) => {
    const todoToComplete = todos.find((todo) => todo.id === id);
    if (todoToComplete) {
      const updatedTodos = todos.filter((todo) => todo.id !== id);
      setTodos(updatedTodos);
      setCompletedTodos((prevCompletedTodos) => [...prevCompletedTodos, todoToComplete]);
    }
  };

  const handleEditTodo = (id: string, newName: string) => {
    const updatedTodos = todos.map((todo) =>
      todo.id === id ? { ...todo, name: newName } : todo
    );
    setTodos(updatedTodos);
  };

  const handleClick = (id: string) => {
    setEditingTodoId(id);
  };

  return (
    <div className='w-[600px] bg-white px-8 py-8 shadow-shadow_2 rounded-[15px]'>
      <h1 className='font-medium text-[20px]'>To-do List</h1>
      <div className='relative mb-8 mt-4'>
        <input
          type='text'
          value={newTodo}
          onChange={(e) => setNewTodo(e.target.value)}
          className='bg-[#46444421] text-[13px] px-2 w-full h-12 rounded-[15px] transition duration-300 focus:border-transparent focus:outline-none'
          placeholder='To-do'
        />
        <button
          onClick={handleAddTodo}
          className='absolute top-[50%] translate-y-[-50%] right-[3%] rounded-[15px] bg-[#83ccc4] text-white font-medium text-[14px] px-6 py-2 '
        >
          Add
        </button>
      </div>

      <ul >
        {todos.map((todo) => (
          <li
            key={todo.id}
            className='bg-[#bdbdbd21] flex justify-between items-center px-2 mb-2 text-[#706969] py-3 rounded-[15px] '
          >
            <div className='flex flex-wrap flex-grow items-center'   onBlur={() => setEditingTodoId(null)} onClick={() => handleClick(todo.id)}>
              {editingTodoId === todo.id ? (
                <>
                  <input
                    type='text'
                    value={todo.name}
                    onChange={(e) => handleEditTodo(todo.id, e.target.value)}
                    className='flex flex-wrap border-none focus:outline-none bg-transparent w-full'
                    ref={inputRef}
                  />
                </>
              ) : (
                <>
                  <input
                    type='checkbox'
                    className='mr-2 w-4 h-4 rounded-full'
                    onChange={() => handleCheckTodo(todo.id)}
                  />
                  <span>{todo.name}</span>
                </>
              )}
            </div>
            <button onClick={() => handleDeleteTodo(todo.id)} className='rounded-full  w-[20px] h-[20px]'>
              x
            </button>
          </li>
        ))}
      </ul>
    </div>
  );
}


