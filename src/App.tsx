import './App.css'
import BoxComplete from './BoxComplete'
import BoxToDo from './BoxToDo'
import Navigation from './Navigation'
import { TodoProvider } from './TodoContext'

function App() {
  return (
    <TodoProvider>
      <div className='font-sans w-full h-screen bg-background_gradient_2'>
        <Navigation />
        <div className='flex my-4 gap-4 justify-center'>
          <div>
            <BoxToDo />
          </div>
          <div>
            <BoxComplete />
          </div>
        </div>
      </div>
    </TodoProvider>
  )
}

export default App
