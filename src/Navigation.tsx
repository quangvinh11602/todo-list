import React from 'react'

export default function Navigation() {
  return (
    <div className='flex items-center px-4 w-full h-16 border-b border-solid border-white'>
      <h1 className=' font-semibold text-[22px] text-white'>To do list</h1>
    </div>
  )
}
