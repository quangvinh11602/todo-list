
import { useTodoContext } from './TodoContext';

export default function BoxComplete() {
  const { completedTodos, setCompletedTodos } = useTodoContext();

  const handleDeleteCompletedTodo = (id: string) => {
    const updatedCompletedTodos = completedTodos.filter((todo) => todo.id !== id);
    setCompletedTodos(updatedCompletedTodos);
  };
  return (
    <div className='w-[600px] bg-white px-8 py-8 shadow-shadow_2 rounded-[15px]'>
      <h1 className='font-medium text-[20px]'>Complete List</h1>
      <ul className='mt-4'>
        {completedTodos.map((completedTodo) => (
          <li key={completedTodo.id}
          className='bg-[#bdbdbd21] flex justify-between items-center px-2 mb-2 text-[#706969] py-3 rounded-[15px] '>
            {completedTodo.name}
            <button
              onClick={() => handleDeleteCompletedTodo(completedTodo.id)}
              className='ml-2 rounded-full w-[20px] h-[20px]'
            >
              x
            </button>
          </li>
        ))}
      </ul>
    </div>
  );
}
